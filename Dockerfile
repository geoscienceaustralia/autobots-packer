FROM ubuntu:bionic
MAINTAINER Tom Butler

RUN apt-get update

# Python
RUN apt-get install -y --no-install-recommends apt-utils
RUN apt-get install -y python3-pip wget unzip jq curl git
RUN pip3 install awscli boto3

# Ruby, and Bundler
RUN apt-get install -y ruby2.5 ruby2.5-dev
RUN gem install bundler

# Packer
# Download packer - most recent version
RUN curl -o packer.zip $(echo "https://releases.hashicorp.com/packer/$(curl -s https://checkpoint-api.hashicorp.com/v1/check/packer | jq -r -M '.current_version')/packer_$(curl -s https://checkpoint-api.hashicorp.com/v1/check/packer | jq -r -M '.current_version')_linux_amd64.zip")

# Unzip and install
RUN unzip packer.zip
RUN mv packer /usr/local/bin/packer
RUN packer -v 
