#Dockerfile for autobots-packer

This repo is used to manage and deploy the autobots-packer Docker image. This Docker image is used for Bitbucket Pipelines which build our AMIs (using Packer).

Current version: v1.1.1
